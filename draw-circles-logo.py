#!/bin/env python3

import sys
import math
import drawsvg as draw

target = None

if "--light" in sys.argv:
    target = "light"
    colorscheme = "light"
    background_color = None
    logo_circle_color = None

if "--dark" in sys.argv:
    target = "dark"
    colorscheme = "dark"
    background_color = None
    logo_circle_color = None

if "--icon" in sys.argv:
    target = "appicon"
    colorscheme = "dark"
    background_color = "darkblue"
    logo_circle_color = None

if "--web" in sys.argv:
    target = "web"
    colorscheme = "dark"
    background_color = None
    #logo_circle_color = "#0c0cb2"
    logo_circle_color = "#7272f0"

if target is None:
    print("Please specify one of:\n  --light for use with light backgrounds\n  --dark for dark backgrounds\n  --icon to create the app icon with an opaque background\n  --web to create pastel icons for the website")
    sys.exit(-1)

d = draw.Drawing(1024, 1024, origin='center')

# Set up our colors

fill_colors = ['skyblue', 'salmon', 'palegreen', 'orchid', 'yellow']

if target == "web":
    R = 240
    r = 160
    line_width = 40
else:
    R = 280  # The big radius from the center of the image to the center of each circle
    r = 200  # The radius of the colored circles
    line_width = 40

if colorscheme == "light":
    fill_alpha = 0.7
    stroke_alpha = 0.95
    background_circle_color = 'white'
    background_circle_alpha = 0.5
    big_ring_color = 'darkblue'
    big_ring_alpha = 0.4
    stroke_colors = ['deepskyblue', 'indianred', 'limegreen', 'mediumorchid', 'gold']
else:
    fill_alpha = 0.5
    stroke_alpha = 0.9
    background_circle_color = 'darkblue'
    background_circle_alpha = 1.0
    big_ring_color = 'white'
    big_ring_alpha = 0.9
    stroke_colors = fill_colors

# If we have a background color, draw it first
if background_color is not None:
    background = draw.Lines(-512,-512,-512,512,512,512,512,-512, fill=background_color)
    d.append(background)

# If we have a logo circle, draw it before we put the main logo on top
if logo_circle_color is not None:
    logo_circle = draw.Circle(0,0,512, stroke='none', fill=logo_circle_color)
    d.append(logo_circle)

# Loop through to draw all the actual colored circles
for i in range(5):
    A = math.pi - i * 2 * math.pi / 5
    x = R * math.cos(A)
    y = R * math.sin(A)

    # Draw the filled circle in a slightly more transparent version of the color
    solid = draw.Circle(x,y,r, stroke='none', fill=fill_colors[i], fill_opacity=fill_alpha)
    d.append(solid)

    # Draw the outline in a slightly more opaque version of the color
    outline = draw.Circle(x,y,r, stroke=stroke_colors[i], stroke_width=line_width, fill='none', stroke_opactiy=stroke_alpha)
    d.append(outline)

# Finally draw the big circle on top that connects all of the smaller colored circles
big_ring = draw.Circle(0,0,264, stroke=big_ring_color, stroke_width=60, fill='none', stroke_opacity=big_ring_alpha)
d.append(big_ring)

# Save to SVG and PNG files
filename = "circles-logo-%s" % target

d.save_svg(filename+".svg")
d.save_png(filename+".png")
