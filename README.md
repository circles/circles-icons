# Circles Icons

This repo contains a simple Python script for generating the Circles icons.

The older icon was hand-drawn and off-center / asymmetrical, and this version
fixes that by using math to make sure every circle is drawn exactly where it
should be.

This version should also be easier to modify going forward.
